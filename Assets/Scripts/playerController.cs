﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class playerController : MonoBehaviour {

    public float walkSpeed = 100.0f;
    public float runSpeed = 150.0f;

    public float turnSmootheningTime = 0.2f;
    float turnSmootheningVelocity;

    public float speedSmootheningTime = 0.1f;
    float speedSmootheningVelocity;
    float currentMovementSpeed;
    

	// Use this for initialization
	void Start () {

        
     }

    // Update is called once per frame
    void Update()
    {

        Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        Vector2 inputDirection = input.normalized;

        if (inputDirection != Vector2.zero)
        {
            float targetedRotation = Mathf.Atan2(inputDirection.x, inputDirection.y) * Mathf.Rad2Deg;
            transform.eulerAngles = Vector3.up * Mathf.SmoothDampAngle(transform.eulerAngles.y, targetedRotation, ref turnSmootheningVelocity, turnSmootheningTime);
        }

        bool runningFlag = Input.GetKey(KeyCode.LeftShift);
        float targetedMovementSpeed = ((runningFlag) ? runSpeed : walkSpeed) * inputDirection.magnitude;

        //if there is a button pressed for movement, inputDirection.magnitude will give a one, else, we get a zero.

        currentMovementSpeed = Mathf.SmoothDamp(currentMovementSpeed, targetedMovementSpeed, ref speedSmootheningVelocity, speedSmootheningTime);

        // speed smoothening is to make the movement seem less clunky.

        transform.Translate(transform.forward * currentMovementSpeed * Time.deltaTime, Space.World);

        // Time.deltaTime is to make the changes with respect to time, not with respect to the frames per second. 

    }
}
