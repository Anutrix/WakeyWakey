﻿using UnityEngine;

public class cameraControllerScript : MonoBehaviour {

    public Transform targetObject;

    public float dampingValue;

    public float smootheningSpeed = 10.0f;
    public Vector3 offsetLocation;

    public void FixedUpdate()
    {
        Vector3 desiredPositionCam = targetObject.position + offsetLocation;
        Vector3 smoothedPositionCam = Vector3.Lerp(transform.position, desiredPositionCam, smootheningSpeed * Time.deltaTime);
        transform.position = smoothedPositionCam;
        Quaternion cameraRotation = Quaternion.LookRotation(targetObject.position - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, cameraRotation, Time.deltaTime * dampingValue);
    }
}
